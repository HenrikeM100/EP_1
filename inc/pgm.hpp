#ifndef PGM_HPP
#define PGM_HPP

#include "imagem.hpp"

class PGM : public Imagem {

private:
	unsigned int cript_cifra;    // Variável para armazenar o número da cifra da criptografia
	PGM();

public:
	PGM(FILE *arquivo);
	~PGM();
	
	void setCript_cifra(unsigned int cript_cifra);
	unsigned int getCript_cifra();

	void Decifrar();
};

#endif