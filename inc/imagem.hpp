#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <string>
#include <vector>
#include <iostream>

using namespace std;

class Imagem {

private:
	unsigned int cript_inic;    // Variável para armazenar o início da criptografia
	unsigned int cript_tam;     // Variável para armazenar o tamanho da criptografia
	vector<unsigned char> matriz;

public:
	Imagem();
	~Imagem();

	void setCript_inic(unsigned int cript_inic);
	unsigned int getCript_inic();

	void setCript_tam(unsigned int cript_tam);
	unsigned int getCript_tam();

	void setMatriz(unsigned char valor);
	unsigned char getMatriz(int i);

	virtual void Decifrar() = 0;
};

#endif