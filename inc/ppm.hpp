#ifndef PPM_HPP
#define PPM_HPP

#include "imagem.hpp"

using namespace std;

class PPM : public Imagem {

private:
	string cript_keyword;    //  String para armazenar a keyword da criptografia
	PPM();

public:
	PPM(FILE *arquivo);
	~PPM();
	
	void setCript_keyword(string cript_keyword);
	string getCript_keyword();

	void Decifrar();
};

#endif