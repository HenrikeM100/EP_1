#include "pgm.hpp"

PGM::PGM(FILE *arquivo) {
  unsigned int inicio, tamanho, cifra;
  unsigned char valor_char;

  fscanf(arquivo," %*c %u %u %u\n", &inicio, &tamanho, &cifra);  //  Ignorando #
  setCript_inic(inicio);
  setCript_tam(tamanho);
  setCript_cifra(cifra);

  fscanf(arquivo,"%*u %*u\n");  //  Ignorando largura e altura
  fscanf(arquivo,"%*u\n");      //  Ignorando valor máximo

  while((fscanf(arquivo, "%c", &valor_char)) != EOF)  // Enquanto houver valor a ser lido
    setMatriz(valor_char);                            // Guarda o valor no vector

  fclose(arquivo);
}

PGM::~PGM() {}
  
void PGM::setCript_cifra(unsigned int cript_cifra) {
  this->cript_cifra = cript_cifra;
}

unsigned int PGM::getCript_cifra() {
  return cript_cifra;
}

void PGM::Decifrar() {
  unsigned int cifra = getCript_cifra() % 26;
  unsigned int inicio = getCript_inic();
  unsigned int tamanho = getCript_tam();
  string codigo;
  string mensagem;


  for(unsigned int i = inicio; i < (inicio + tamanho); ++i)
    codigo.push_back(getMatriz(i));  // Codigo criptografado é armazenado aqui

  for(unsigned int i = 0; i < tamanho; ++i) {  // Processo de conversão
    if(codigo[i] > 64 && codigo[i] < 91) {  // Se for uma letra maiúscula
      if(codigo[i] - cifra < 65)  // "Dando a volta" no alfabeto
        mensagem.push_back(codigo[i] - cifra + 26);
      else
       mensagem.push_back(codigo[i] - cifra);
    }
    else if(codigo[i] > 96 && codigo[i] < 123) {  // Se for uma letra minúscula
      if(codigo[i] - cifra < 97)  // "Dando a volta" no alfabeto
        mensagem.push_back(codigo[i] - cifra + 26);
      else
        mensagem.push_back(codigo[i] - cifra);
    }
    else  //  Se não for uma letra
      mensagem.push_back(codigo[i]);
  }

  cout << endl << "Mensagem Decifrada: " << endl;
  cout << mensagem << endl;
}