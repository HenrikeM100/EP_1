/******************************************
 *                                        *
 *       Exercício de Programação 1       *
 *                                        *
 ******************************************
 *                                        *
 *   Universidade de Brasília             *
 *                                        *
 *   Aluno: Henrique Martins de Messias   *
 *   Matrícula: 17/0050394                *
 *   Disciplina: Orientação a Objetos     *
 *   Professor: Renato Coral Sampaio      *
 *                                        *
 ******************************************/

#include <stdio.h>
#include <string.h>
#include "imagem.hpp"
#include "ppm.hpp"
#include "pgm.hpp"

using namespace std;

int main() {
  char nome_arquivo[101];
  bool valido = false;
  string tipo;
  FILE *arquivo;
  char codigo[101];

  cout << "Insira o nome ou o local da imagem: ";

  while(!valido) {    // Tenta abrir arquivo até um que consiga
  	scanf(" %[^\n]", nome_arquivo);

  	try {
  		if((arquivo = fopen(nome_arquivo, "r")) == NULL)    //  Se arquivo não for encontrado
			throw 0;
		else {
			fscanf(arquivo,"%s\n", codigo);
			if(strcmp(codigo, "P6") != 0 && strcmp(codigo, "P5") != 0) {    // Se arquivo não for do tipo certo
				fclose(arquivo);
				throw -1;
			}
		}
		cout << endl << "Arquivo encontrado." << endl;
		valido = true;
	} catch (int erro) {
		if (erro == -1)
			cout << "Tipo inválido, tente outro arquivo: ";
		else if (erro == 0)
			cout << "Arquivo não encontrado, tente novamente: ";
	}
  }

  if(strcmp(codigo, "P6") == 0) {
    PPM imagens_PPM(arquivo);
    imagens_PPM.Decifrar();
  }
  else if(strcmp(codigo, "P5") == 0) {
    PGM imagens_PGM(arquivo);
    imagens_PGM.Decifrar();
  }
  else
  	cout << "Erro desconhecido." << endl;

  cout << endl << "Programa encerrado." << endl;
 
  return 0;
}