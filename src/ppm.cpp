#include "ppm.hpp"
#include <unordered_map>

PPM::PPM(FILE *arquivo) {
  unsigned int inicio, tamanho;
  char keyword_char[27];
  unsigned char valor_char;

  fscanf(arquivo," %*c %u %u %s\n", &inicio, &tamanho, keyword_char);  //  Ignorando #
  setCript_inic(inicio);
  setCript_tam(tamanho);

  string keyword(keyword_char);
  setCript_keyword(keyword);

  fscanf(arquivo,"%*u %*u\n");  //  Ignorando largura e altura
  fscanf(arquivo,"%*u\n");      //  Ignorando valor máximo

  while((fscanf(arquivo, "%c", &valor_char)) != EOF)  // Enquanto houver valor a ser lido
    setMatriz(valor_char);                            // Guarda o valor no vector

  fclose(arquivo);
}

PPM::~PPM() {}
  
void PPM::setCript_keyword(string cript_keyword) {
  this->cript_keyword = cript_keyword;
}

string PPM::getCript_keyword() {
  return cript_keyword;
}

void PPM::Decifrar() {
  unsigned int inicio = getCript_inic();
  unsigned int tamanho = inicio + (getCript_tam()*3);
  string cifra = getCript_keyword();
  unsigned int i;
  string mensagem;

  unordered_map<char, char> cifra_alfab;  // Guarda a cifra e o alfabeto original, nessa ordem

  for(i = 0; i < cifra.size(); ++i)  // Relação entre cifra e alfabeto
    cifra_alfab[cifra[i]] = 97+i;

  while(cifra_alfab.size() < 26) {   				   //////////////////////////////////////
    for(char j = 97; j < 123; ++j)					   //  Relação entre as letras do      //
      if(cifra_alfab.find(j) == cifra_alfab.end()) {   //  alfabeto não usadas na cifra    //
        cifra_alfab[j] = 97+i;						   //  com o alfabeto original         //
        i++;										   //////////////////////////////////////
      }																	
  }

  for(i = inicio; i < tamanho; i += 3) {   //  Para cada pixel...
    int R = getMatriz(i) % 10;
    int G = getMatriz(i+1) % 10;	// Pegar ultimo algarismo de cada valor RGB
    int B = getMatriz(i+2) % 10;

    char total = R+G+B + 96;  //  Associando a posição da letra com a tabela ASCII

    if(cifra_alfab.find(total) != cifra_alfab.end())    //  Para cada letra criptografada encontrada
      mensagem.push_back(cifra_alfab[total]);           //  Armazenar a letra decifrada
    else if(total == 96)         //  Se R+G+B = 0
      mensagem.push_back(32);    //  Coloca espaço
  }

  cout << endl << "Mensagem Decifrada: " << endl;
  cout << mensagem << endl;
}