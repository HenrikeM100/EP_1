#include "imagem.hpp"

Imagem::Imagem() {
	cript_inic = 0;
	cript_tam = 0;
}

Imagem::~Imagem() {}

void Imagem::setCript_inic(unsigned int cript_inic) {
  this->cript_inic = cript_inic;
}

unsigned int Imagem::getCript_inic() {
  return cript_inic;
}

void Imagem::setCript_tam(unsigned int cript_tam) {
  this->cript_tam = cript_tam;
}

unsigned int Imagem::getCript_tam() {
  return cript_tam;
}

void Imagem::setMatriz(unsigned char valor) {
  matriz.push_back(valor);
}

unsigned char Imagem::getMatriz(int i) {
	return matriz[i];
}